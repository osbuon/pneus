using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Microsoft.MixedReality.Toolkit.Experimental.UI;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Scripting;

public class GDMoEiC : MonoBehaviour
{
    [SerializeField]
    Transform Player;
    [SerializeField]
    GameObject Spotlight;
    [SerializeField]
    Material Conform;
    [SerializeField]
    Material Error;
    [SerializeField]
    Material Noscan;
    [SerializeField]
    Material Waiting;
    [SerializeField]
    GameObject Screen;
    [SerializeField]
    GameObject FatherOfTires;
    [SerializeField]
    TMP_Text MainText;
    [SerializeField]
    NonNativeKeyboard Keyboard;
    [SerializeField]
    PathLine PathLine;

    [SerializeField]
    public int PneusAmount = 10;
    [SerializeField]
    public float PneusValid = 0.7f;
    [SerializeField]
    public float PneusAutomaticDetect = 0.7f;

    private int BenValid = 0;
    public float Score { get; set; } = 0;
    private Dictionary<string, float> scores = new Dictionary<string, float>();
    enum State { Waiting, Pneus, Grue, EnterName, Finished }
    private State state = State.Waiting;
    private GameObject uiInteractor;

    public static GDMoEiC Instance
    {
        get; private set;
    }

    void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        uiInteractor = Player.Find("Camera Offset").Find("Right Controller").Find("UI Interactor").gameObject;

        EventManager.StartListening("PneuAutoDetectEnter", (EventParam param) =>
        {
            PneuAutoDetectEnter(((AutoDetectPneu.EventParamPneu)param).pneu);
        });
        EventManager.StartListening("PneuAutoDetectExit", (EventParam param) =>
        {
            PneuAutoDetectExit(((AutoDetectPneu.EventParamPneu)param).pneu);
        });
        EventManager.StartListening("CodeBarDetected", (EventParam param) =>
        {
            CodeBarDetected(((AutoDetectPneu.EventParamPneu)param).pneu);
        });

        EventManager.StartListening("TargetOkEntered", (EventParam param) =>
        {
            TargetOkEntered(((AutoDetectPneu.EventParamPneu)param).pneu);
        });
        EventManager.StartListening("TargetOkExit", (EventParam param) =>
        {
            TargetOkExit(((AutoDetectPneu.EventParamPneu)param).pneu);
        });

        EventManager.StartListening("TargetKoEntered", (EventParam param) =>
        {
            TargetKoEntered(((AutoDetectPneu.EventParamPneu)param).pneu);
        });
        EventManager.StartListening("TargetKoExit", (EventParam param) =>
        {
            TargetKoExit(((AutoDetectPneu.EventParamPneu)param).pneu);
        });

        EventManager.StartListening("RedBPressed", (EventParam param) =>
        {
            StopGame();
        });
        EventManager.StartListening("GreenBPressed", (EventParam param) =>
        {
            if (state == State.Waiting)
            {
                EventManager.TriggerEvent("PneusStart");
                state = State.Pneus;
            }
        });

        EventManager.StartListening("PathStart", (EventParam param) =>
        {
            if (state == State.Waiting || state == State.Finished)
            {
                EventManager.TriggerEvent("GeneratePath");
                state = State.Grue;
            }
        });
        EventManager.StartListening("PathEnd", (EventParam param) =>
        {
            if (state == State.Grue)
            {
                EventManager.TriggerEvent("CleanPath");
                state = State.EnterName;
            }
        });

        Keyboard.OnTextSubmitted += (object sender, EventArgs args) =>
        {
            scores[Keyboard.InputField.text] = Score;
            state = State.Finished;
        };
        KeyboardActive = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (state == State.Waiting)
        {
            if (KeyboardActive)
                KeyboardActive = false;
            MainText.text = "";
            BenValid = 0;
            Score = 0;
        }
        else if (state == State.Pneus)
        {
            MainText.text = "Temps : " + Score;
            Score += Time.deltaTime;
            if (BenValid == PneusAmount)
            {
                state = State.EnterName;
            }
        }
        else if (state == State.Grue)
        {
            MainText.text = "Temps : " + Score;
            if (PathLine.Count == 0)
            {
                Score += Time.deltaTime;
            }
        }
        else if (state == State.EnterName)
        {
            if (!KeyboardActive)
                KeyboardActive = true;
        }
        else if (state == State.Finished)
        {
            if (KeyboardActive)
                KeyboardActive = false;
            Score = 0;

            MainText.text = "Temps :\n";
            foreach (var item in scores)
                MainText.text += item.Key + ": " + item.Value + "\n";
        }
    }

    void PneuAutoDetectEnter(Pneu pneu)
    {
        StartCoroutine(SpotScan());
        if (pneu.AutomaticDetect)
        {
            if (pneu.Valid)
                Screen.GetComponent<Renderer>().material = Conform;
            else
                Screen.GetComponent<Renderer>().material = Error;
        }
        else Screen.GetComponent<Renderer>().material = Noscan;
    }

    void PneuAutoDetectExit(Pneu pneu)
    {
        Screen.GetComponent<Renderer>().material = Waiting;
    }

    void CodeBarDetected(Pneu pneu)
    {
        if (pneu.Valid)
            Screen.GetComponent<Renderer>().material = Conform;
        else
            Screen.GetComponent<Renderer>().material = Error;
        StartCoroutine(ReturnToWait());
    }

    IEnumerator SpotScan()
    {
        Spotlight.GetComponent<Light>().enabled = true;
        yield return new WaitForSeconds(0.5f);
        Spotlight.GetComponent<Light>().enabled = false;
    }

    IEnumerator ReturnToWait()
    {
        yield return new WaitForSeconds(4f);
        Screen.GetComponent<Renderer>().material = Waiting;
    }

    void TargetOkEntered(Pneu pneu)
    {
        if (pneu.Valid) BenValid += 1;
    }

    void TargetOkExit(Pneu pneu)
    {
        if (pneu.Valid) BenValid -= 1;
    }

    void TargetKoEntered(Pneu pneu)
    {
        if (!pneu.Valid) BenValid += 1;
    }

    void TargetKoExit(Pneu pneu)
    {
        if (!pneu.Valid) BenValid -= 1;
    }

    private bool keyboardActive = false;
    public bool KeyboardActive
    {
        get
        {
            return keyboardActive;
        }
        set
        {
            keyboardActive = value;
            Keyboard.gameObject.SetActive(value);
            uiInteractor.SetActive(value);
        }
    }

    void StopGame()
    {
        foreach (Transform child in FatherOfTires.transform)
        {
            Destroy(child.gameObject);
        }
        Screen.GetComponent<Renderer>().material = Waiting;
        state = State.Waiting;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeReader : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponentInParent<Pneu>())
            EventManager.TriggerEvent("CodeBarDetected", new AutoDetectPneu.EventParamPneu(other.GetComponentInParent<Pneu>()));
    }

}

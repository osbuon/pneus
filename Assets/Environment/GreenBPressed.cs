using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class GreenBPressed : MonoBehaviour
{
    public void OnHover(HoverEnterEventArgs args)
    {
        EventManager.TriggerEvent("GreenBPressed");
    }
}

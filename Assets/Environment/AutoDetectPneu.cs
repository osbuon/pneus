using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDetectPneu : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Pneu>())
            EventManager.TriggerEvent("PneuAutoDetectEnter", new EventParamPneu(other.GetComponent<Pneu>()));
    }

    void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Pneu>())
            EventManager.TriggerEvent("PneuAutoDetectExit", new EventParamPneu(other.GetComponent<Pneu>()));
    }

    public class EventParamPneu : EventParam
    {
        public EventParamPneu(Pneu pneu)
        {
            this.pneu = pneu;
        }
        public Pneu pneu { get; set; }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPneus : MonoBehaviour
{
    [SerializeField]
    GameObject Pneu;
    [SerializeField]
    GameObject Father;

    void Start()
    {
        EventManager.StartListening("PneusStart", (EventParam ev) =>
        {
            StartCoroutine(CreatePneus());
        });
    }

    IEnumerator CreatePneus()
    {
        for (int i = 0; i < GDMoEiC.Instance.PneusAmount; i++)
        {
            var pneu = Instantiate(Pneu, Father.transform).GetComponent<Pneu>();
            pneu.Valid = Random.Range(0f, 1f) < GDMoEiC.Instance.PneusValid;
            pneu.AutomaticDetect = Random.Range(0f, 1f) < GDMoEiC.Instance.PneusAutomaticDetect;
            yield return new WaitForSeconds(0.1f);
        }
    }

}

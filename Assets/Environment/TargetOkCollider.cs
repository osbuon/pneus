using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetOkCollider : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Pneu>())
            EventManager.TriggerEvent("TargetOkEntered", new AutoDetectPneu.EventParamPneu(other.GetComponent<Pneu>()));
    }

    void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Pneu>())
            EventManager.TriggerEvent("TargetOkExit", new AutoDetectPneu.EventParamPneu(other.GetComponent<Pneu>()));
    }
}

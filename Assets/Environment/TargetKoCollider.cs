using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetKoCollider : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Pneu>())
            EventManager.TriggerEvent("TargetKoEntered", new AutoDetectPneu.EventParamPneu(other.GetComponent<Pneu>()));
    }

    void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Pneu>())
            EventManager.TriggerEvent("TargetKoExit", new AutoDetectPneu.EventParamPneu(other.GetComponent<Pneu>()));
    }
}

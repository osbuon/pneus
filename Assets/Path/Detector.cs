using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detector : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        var path = transform.parent.parent.GetComponent<PathLine>();
        path.Count++;
    }

    void OnTriggerExit(Collider other)
    {
        var path = transform.parent.parent.GetComponent<PathLine>();
        path.Count--;
    }
}

using System.Threading;
using UnityEngine;

public class PathLine : MonoBehaviour
{
    public Material material;

    // nombre de segments en collision avec le détecteur de la grue
    public int Count { get; set; }
    private Transform start;
    private Transform end;
    private Transform segments;

    void Start()
    {
        start = transform.Find("Start");
        end = transform.Find("End");
        segments = transform.Find("Segments");

        end.gameObject.SetActive(false);

        EventManager.StartListening("GeneratePath", (EventParam param) =>
        {
            Generate(5, Vector3.forward * 0.5f, new Vector3(1, 0, 0) * 0.2f);
        });

        EventManager.StartListening("CleanPath", (EventParam param) =>
        {
            Clean();
        });
    }

    public void Generate(int n, Vector3 direction, Vector3 randomness)
    {
        start.gameObject.SetActive(false);

        Vector3 pos = transform.position;
        for (int i = 0; i < n; i++)
        {
            Vector3 new_pos = pos + direction + new Vector3(Random.Range(-1, 1) * randomness.x, Random.Range(-1, 1) * randomness.y, Random.Range(-1, 1) * randomness.z);

            var obj = new GameObject();
            obj.transform.SetParent(segments);
            obj.transform.position = (pos + new_pos) / 2;
            obj.transform.localScale = new Vector3(0.1f, (new_pos - pos).magnitude, 0.1f);
            obj.transform.up = new_pos - pos;

            var renderer = obj.AddComponent<LineRenderer>();
            renderer.widthMultiplier = 0.05f;
            renderer.material = material;
            renderer.positionCount = 2;
            renderer.SetPosition(0, pos);
            renderer.SetPosition(1, new_pos);

            var collider = obj.AddComponent<BoxCollider>();
            collider.isTrigger = true;
            collider.center = new Vector3();
            collider.size = new Vector3(1, 1, 1);

            var detector = obj.AddComponent<Detector>();

            pos = new_pos;
        }

        end.position = pos;
        end.gameObject.SetActive(true);
    }

    public void Clean()
    {
        end.gameObject.SetActive(false);
        foreach (Transform child in segments)
        {
            Destroy(child.gameObject);
        }
        start.gameObject.SetActive(true);
    }
}
